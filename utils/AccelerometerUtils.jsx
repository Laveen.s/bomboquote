import { Accelerometer } from 'expo-sensors';

export const configureAccelerometer = (callback) => {
  Accelerometer.setUpdateInterval(500);

  const subscription = Accelerometer.addListener(accelerometerData => {
    const { x, y, z } = accelerometerData;
    const magnitude = Math.sqrt(x * x + y * y + z * z);
    const threshold = 1.5;

    callback(magnitude > threshold);
  });

  return subscription; 
};
