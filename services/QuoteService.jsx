import axios from 'axios';
import { API_KEY } from '../config/config';

export const fetchQuote = async () => {
  try {
    const response = await axios.get('https://api.api-ninjas.com/v1/quotes?category=happiness', {
      headers: { 'X-Api-Key': API_KEY }
    });
    if (response.data && response.data.length > 0) {
      return response.data[0].quote;
    }
  } catch (error) {
    console.error(error);
    throw error;
  }
};
