import React, { useEffect, useState } from 'react';
import { View } from 'react-native';
import QuoteDisplay from './components/QuoteDisplay';
import { fetchQuote } from './services/QuoteService';
import { speak } from './services/SpeechService';
import { configureAccelerometer } from './utils/AccelerometerUtils';

export default function App() {
  const [quote, setQuote] = useState(null);
  const [loading, setLoading] = useState(true);
  const [cooldown, setCooldown] = useState(false);
  const [lastShake, setLastShake] = useState(0);

  useEffect(() => {
    getAndSetQuote();

    const subscription = configureAccelerometer(isShaking => {
      const currentTime = new Date().getTime();
      if (isShaking && !cooldown && currentTime - lastShake > 30000) {
        setCooldown(true);
        setLastShake(currentTime);
        getAndSetQuote();
        setTimeout(() => setCooldown(false), 30000);
      }
    });

    return () => subscription.remove();
  }, []);

  const getAndSetQuote = async () => {
    setLoading(true);
    try {
      const newQuote = await fetchQuote();
      setQuote(newQuote);
      speak(newQuote);
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  };

  return (
    <View style={{ flex: 1 }}>
      <QuoteDisplay quote={quote} loading={loading} />
    </View>
  );
}
