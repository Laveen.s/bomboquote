import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator } from 'react-native';

export default function QuoteDisplay({ quote, loading }) {
  return (
    <View style={styles.container}>
      {loading ? (
        <ActivityIndicator size="large" color="#0000ff" />
      ) : (
        <Text style={styles.quote}>{quote}</Text>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    padding: 20,
  },
  quote: {
    fontSize: 24,
    textAlign: 'center',
  },
});
